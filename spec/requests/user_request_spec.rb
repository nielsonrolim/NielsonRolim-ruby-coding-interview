require 'rails_helper'

RSpec.describe "Users", type: :request do

  RSpec.shared_context 'with multiple companies' do
    let!(:company_1) { create(:company) }
    let!(:company_2) { create(:company) }

    before do
      5.times do
        create(:user, company: company_1)
      end
      5.times do
        create(:user, company: company_2)
      end
    end
  end

  describe "#index" do
    let(:result) { JSON.parse(response.body) }

    context 'when fetching users by company' do
      include_context 'with multiple companies'

      it 'returns only the users for the specified company' do
        get company_users_path(company_1)

        expect(result.size).to eq(company_1.users.size)
        expect(result.map { |element| element['id'] } ).to eq(company_1.users.ids)
      end
    end

    context 'when fetching all users' do
      include_context 'with multiple companies'

      it 'returns all the users' do

      end
    end

    context 'when fetching users by username' do
      include_context 'with multiple companies'

      let!(:user1) { create(:user, username: 'user1', company_id: company_1.id) }

      context 'when filtering by the entire username' do
        it 'returns user1' do
          get company_users_path(company_1, params: { username: user1.username }, format: :json)

          expect(result.size).to eq(1)
          expect(result.first['username']).to eq(user1.username)
        end
      end

      context 'when filtering by part of the username' do
        let!(:user2) { create(:user, username: 'user2', company_id: company_1.id) }
        let!(:user3) { create(:user, username: 'user3', company_id: company_1.id) }

        it 'returns three users' do
          get company_users_path(company_1, params: { username: 'user' }, format: :json)

          expect(result.size).to eq(3)
          expect(result.first['username']).to eq(user1.username)
          expect(result.second['username']).to eq(user2.username)
          expect(result.third['username']).to eq(user3.username)
        end
      end
    end
  end
end
