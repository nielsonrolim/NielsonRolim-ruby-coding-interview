class UsersController < ApplicationController

  def index
    users = User
              .by_company(params[:company_id])
              .by_username(search_params[:username])

    render json: users.all
  end

  def show
    user = User.find_by(username: params[:username])
    render json: user
  end

  private

  def search_params
    params.permit(:username)
  end

end
